**Journal des modifications**

**0.1**  
- Classe de modélisation : **modelisation.affine** et **modelisation.lineaire**

**0.2**  
- Fonction de calcul d'incertitude de type A : **uA**  
- Fonction d'importation de fichier Latis:  **importLatis**  

**0.3**  
- Fonction d'importation de fichier Phyphox (Autocorrélation audio):  **importPhyphox**  
- Fonction de calcul d'un spectre en fréquence par fft : **specFreq**  

**0.4**  
- Fonction d'affichage d'un tableau d'avancement (dans un notebook)

**0.5**  
- Ouvre les fichiers d'analyse cinématique de Fizziq  
- Classe **fizziq**

**0.6**  
- Ouvre les fichiers du logiciel VisualSpectra Jr (spectrophotomètres OVIO)  
- Classe **visualSpectra**, méthodes **importSpectre**, **importAnalyse** et **importCinetique**
