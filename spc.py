"""
Bibliothèque Python pour les sciences physiques et chimiques
Version: 0.6
Author: LLA (Lycée Louis Armand - Mulhouse)
License: GPL v3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""

def tableau_avancement(reactifs, produits, xmax = True, unite = 'mol'):
    """
    Affiche un tableau d'avancement au format HTML dans un notebook.
    Insérer $$\require{mhchem}$$ dans une cellule markdown pour activer l'affichage des formules chimiques.

    Parameters:
        reactifs (list of lists): liste des réactifs  au format ['formule', ni, nf]
        produits (list of lists): liste des produits au format ['formule', ni, nf]
        xmax (bool): affiche xmax au lieu de xf
        unite (str): unité affichée, par défaut 'mol'
        
    Example:
        reactifs = [["CH4", 2.5, 0], ["2O2", 5, 0]]
        produits = [["CO2", 0, 2.5], ["2H2O", 0, 5]]
        tableau_avancement(reactifs, produits)
    """
    try :
        from IPython.display import HTML, display
    except :
        print("Cette fonction n'est utilisable que dans un notebook")
        
    xfmax = 'max' if xmax else 'f'
    data = [['', 'Etat initial', 'Etat final'],
            ['<center>Avancement<br>('+unite+')', '<center>$$x=0$$', '<center>$$x=x_{'+xfmax+'}$$']]
    for i,r in enumerate(reactifs) :
        data.append(["$$\ce{"+r[0]+"}$$", "$$"+str(r[1])+"$$", "$$"+str(r[2])+"$$"])
        if i < len(reactifs)-1:
            data.append(['+', '', ''])
    data.append(['🠒', '', ''])
    for j,p in enumerate(produits) :
        data.append(["$$\ce{"+p[0]+"}$$", "$$"+str(p[1])+"$$", "$$"+str(p[2])+"$$"])
        if j < len(produits)-1:
            data.append(['+', '', ''])
    html = "<table>"
    for row in list(map(list, zip(*data))):
        html += "<tr>"
        for field in row:
            html += "<td><h4>%s</h4></td>"%(field)
        html += "</tr>"
    html += "</table>"
    display(HTML(html))

def specFreq(t, y, T = None):
    """
    Génère les données pour tracer spectre en fréquence par transformée de Fourier.

    Parameters:
        t (list, numpy array): temps (s)
        y (list, numpy array): amplitude (u.a.)
        
    Returns:
        f, a (tuple of numpy arrays): fréquences et amplitudes des harmoniques
    """
    import numpy as np
    if not T :
        T_s = t[1]-t[0]
    fft_y = np.fft.fft(y)
    n = len(fft_y)
    freq = np.fft.fftfreq(n, T_s)
    half_n = int(np.ceil(n/2.0))
    fft_y_half = (2.0 / n) * fft_y[:half_n]
    freq_half = freq[:half_n]
    return(freq_half, np.abs(fft_y_half))



def importPhyphox(fichier):
    """
    Importe un fichier ZIP généré par Phyphox 'Autocorrélation Audio'.

    Parameters:
        fichier (str): nom du fichier .zip généré par phyphox
        
    Returns:
        t, y (tuple of numpy arrays): temps (s) et amplitude (ua)
    """
    from io import TextIOWrapper
    from zipfile import ZipFile
    from numpy import loadtxt
    with ZipFile(fichier) as zf:
        with zf.open("Raw data.csv", 'r') as infile:
            t, y = loadtxt(TextIOWrapper(infile, 'utf-8'), delimiter=',', unpack=True, skiprows=1)
    t = t*1E-3 # Conversion des millisecondes en secondes
    return(t,y)


def uA(data):
    """
    Calcule la moyenne et l'incertitude-type d'un échantillon de mesures.

    Parameters:
        data (list, tuple): mesures
        
    Returns:
        m, i (tuple): moyenne et incertitude-type
    """
    from statistics import mean, pstdev
    from math import sqrt
    try :
        iter(data)
        return (mean(data), pstdev(data)/sqrt(len(data)-1))
    except :
        print("Data non iterable")

def importLatis(fichier):
    """
    Importe un fichier CSV ou TXT généré par Latis avec autodétection du format.

    Parameters:
        fichier (str): nom du fichier .csv ou .txt de Latis
    
    Returns:
        data (list): Liste représentant chacune des colonnes
    """
    with open(fichier) as f :
        header = f.readline().rstrip("\n") # lit la première ligne
        sep = ";" if ";" in header else "\t" # point-virgule comme séparateur de champ sinon tabulation
        nb_col = len(header.split(sep)) # détermine le nombre de colonnes d'après l'en-tête
        reader = f.readlines() # lit les lignes suivantes jusqu'à la fin
        data = [[] for i in range(nb_col)] # Crée une liste de listes
        for line in reader :
            newline = line.rstrip('\n').split(sep) # retire le caractère de fin de ligne et sépare les champs
            for i, dat in enumerate(newline) :
                data[i].append(float(dat.replace(',', '.'))) # ajoute un élément de chaque colonne dans chaque liste, converti en float
    return(data)


class modelisation:
    #https://commandlinefanatic.com/cgi-bin/showarticle.cgi?article=art084
    """
    Modélisation simple d'un nuage de points par une fonction affine ou linéaire.
    """
    def affine(X, Y):
        """
        Modélisation affine : Calcule le coefficient directeur et l'ordonnée à l'origine du modèle affine.

        Parameters:
            X (list, tuple):  valeurs en abscisses
            Y (list, tuple):  valeurs en ordonnées
        
        Returns:
            a, b (tuple): coefficient directeur et ordonnée à l'origine
        """
        long = len(X)
        ave_x = sum([X[i] for i in range(long)]) / long
        ave_y = sum([Y[i] for i in range(long)]) / long

        a = sum([X[l] * (Y[l] - ave_y) for l in range(long)]) / sum([X[l] * (X[l] - ave_x) for l in range(long)])
        b = ave_y - a * ave_x
        return (a, b)

    def lineaire(X, Y):
        """
        Modélisation linéaire : Calcule le coefficient directeur du modèle linéaire.
        
        Parameters:
            X (list, tuple):  valeurs en abscisses
            Y (list, tuple):  valeurs en ordonnées
        
        Returns:
            a (float): coefficient directeur
        """
        long = len(X)
        a = sum([X[l]*Y[l] for l in range(long)])/sum([X[l]*X[l] for l in range(long)])
        return a

class fizziq:
    """
    Ouvre les fichiers générés par l'application Fizziq.
    """
    def kinematic(txt_file):
        """
        Ouvre un fichier exporté par l'ananlyse cinématique de Fizziq.
        
        Parameters:
            txt_file:  fichier texte généré par Fizziq
        
        Returns:
            (t,x,y): Vecteurs Numpy contenant le temps, les abscisses et les ordonnées
        """
        import numpy as np
        with open(txt_file, 'r') as f:
            for l in f.readlines():
                if l.startswith('ref'):
                    t = np.fromstring(l[l.find("[")+1:l.find("]")], dtype=float, sep=',')
                elif l.startswith('x'):
                    x = np.fromstring(l[l.find("[")+1:l.find("]")], dtype=float, sep=',')
                elif l.startswith('y'):
                    y = np.fromstring(l[l.find("[")+1:l.find("]")], dtype=float, sep=',')
        return t,x,y
            
class visualSpectra:
    """
    Ouvre les fichiers générés par le logiciel VisualSpectra Jr des spectrophotomètres OVIO
    """
    
    def importSpectre(fichier):
        """
        Ouvre un spectre :
        fichier .absorb : Absorbance en fonction de la longueur d'onde (nm)
        fichier .trans : Transmittance (%) en fonction de la longueur d'onde (nm)
        fichier .sample : Intensité (unité arb.) en fonction de la longueur d'onde (nm)
        
        Parameters:
            fichier:  fichier généré par VisualSpectra
        
        Returns:
            x,y: Listes contenant la longueur d'onde et
                 l'intensité, la transmittance ou l'absorbance
        """
        x = []
        y = []
        try : 
            with open(fichier, 'r', encoding = "cp1252") as f:
                for i in range(15) :
                    f.readline()
                while True:
                    testline = f.readline()
                    if len(testline) ==0:
                        break
                    lo = float(testline[11:18])
                    i = float(testline[19:])
                    x.append(lo)
                    y.append(i)
        except :        
            return None, None
        return x, y
    
    def importAnalyse(fichier):
        """
        Ouvre un fichier d'analyse quantitative (loi de Beer-Lambert)
        
        Parameters:
            fichier:  fichier texte (.txt) généré par VisualSpectra
        
        Returns:
            x,y: Listes contenant la longueur d'onde et
                 l'intensité, la transmittance ou l'absorbance
        """
        x = []
        y = []
        try : 
            with open(fichier, 'r', encoding = "cp1252") as f:
                f.readline() # l.o.
                for i in range(5) :
                  line = f.readline()
                  if not line == '-100, -100\n' :
                      if not line == '0, 0\n':
                            x.append(float(line.split(',')[0]))
                            y.append(float(line.split(',')[1].split()[0]))
        except :        
            return None, None
        return x,y

    def importCinetique(fichier):
        """
        Ouvre un fichier de suivi cinétique, option "Write selected wavelength" uniquement.
        
        Parameters:
            fichier:  fichier texte (.txt) généré par VisualSpectra
        
        Returns:
            t, Aa [, Ab]: Listes contenant le temps et la ou les absorbances
        """
        def hms2s(s):
            l = s.split(':')
            return int(l[0]) * 3600 + int(l[1]) * 60 + int(l[2])
        
        Aa = []
        Ab = []
        t = []
        try : 
            with open(fichier, 'r', encoding = "cp1252") as f:
                a =f.readline()
                #lambdaA = a.split()[2].split('*')[0]
                lambdaB = a.split()[4].split('*')[0]
                if lambdaB == '0nm':
                    nbCol = 1
                else :
                    nbCol = 2
                tps0 = 0
                while True:
                    testline = f.readline()
                    if len(testline) ==0:
                        break
                    if testline[0] != " ":
                        tps=hms2s(testline[0:8])
                        if tps0 == 0 :
                            tps0 = tps
                        t.append(tps-tps0)
                        if nbCol == 1:
                            Aa.append(float(testline.split()[-1])/1000)
                        else :
                            Ab.append(float(testline.split()[-1])/1000)
                            Aa.append(float(testline.split()[-2])/1000)
        except :        
            return None, None
        
        if nbCol == 1 : 
            return t, Aa
        else : 
            return t, Aa, Ab
